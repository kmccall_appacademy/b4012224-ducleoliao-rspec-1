def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, freq=2)
  count = 0
  result = []
  until count == freq
    result << str
    count +=1
  end
  result.join(" ")
end

def start_of_word(str, num)
  str[0...num]
end

def first_word(str)
  str.split(" ").first
end

def titleize(str)
  exceptions = ["and", "the", "over"]
  result = []
  return str.capitalize unless str.split(" ").count > 1
  sentences = str.split(" ")
  sentences.map.with_index do |word, idx|
    if idx == 0
      result << word.capitalize
    elsif exceptions.include?(word)
      result << word
    else
      result << word.capitalize
    end
  end
  result.join(" ")
end
