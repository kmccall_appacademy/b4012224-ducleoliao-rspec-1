def translate(str)
  str.split(" ").map {|el| piglatin(el)}.join(" ")
end

def piglatin(word)
  vowels = ['a', 'e', 'i', 'o', 'qu']
  until vowels.include?(word[0])
    word = word[1..-1] + word[0]
  end
  word + "ay"
end
