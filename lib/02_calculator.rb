def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  return 0 if array == []
  array.inject(:+)
end

def multiply(array)
  array.inject(:*)
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 0 if num == 0
  (1..num).inject(:*)
end
